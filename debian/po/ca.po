#
# Catalan translation for foomatic-filters package.
# Copyright (C) 2007 Chris Lawrence.
# This file is distributed under the same license as the foomatic-filters
# package.
#
# Jordà Polo <jorda@ettin.org>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0.2-20061031-1.2\n"
"Report-Msgid-Bugs-To: foomatic-filters@packages.debian.org\n"
"POT-Creation-Date: 2010-08-10 18:01+0200\n"
"PO-Revision-Date: 2007-10-08 14:27+0200\n"
"Last-Translator: Jordà Polo <jorda@ettin.org>\n"
"Language-Team: Català <debian-l10n-catalan@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#: ../foomatic-filters.templates:2001
msgid "Foomatic Printer Filter Configuration"
msgstr ""

#. Type: boolean
#. Description
#: ../foomatic-filters.templates:3001
msgid "Enable logging debug output into a log file (INSECURE)?"
msgstr ""
"Voleu activar el registre dels missatges de depuració en un fitxer (NO "
"SEGUR)?"

#. Type: boolean
#. Description
#: ../foomatic-filters.templates:3001
msgid ""
"If you choose this option, the log file will be named /tmp/foomatic-rip.log."
msgstr ""
"Si trieu aquesta opció, el fitxer de registre s'anomenarà /tmp/foomatic-rip."
"log."

#. Type: boolean
#. Description
#: ../foomatic-filters.templates:3001
msgid ""
"This option is a potential security issue and should not be used in "
"production. However, if you are having trouble printing, you should enable "
"it and include the log file in bug reports."
msgstr ""
"Aquesta opció representa un risc potencial de seguretat i no s'hauria "
"d'utilitzar en entorns en producció. Tanmateix, si teniu problemes a l'hora "
"d'imprimir, l'hauríeu d'activar i incloure el fitxer de registre a l'informe "
"d'error."

#. Type: select
#. Choices
#: ../foomatic-filters.templates:4001
msgid "Automagic"
msgstr "Automàgic"

# NOTA: S'utilitza una forma neutra per evitar -t/-da, ja que la mateixa
# cadena s'aprofita en un context diferent. -- Jordà
#. Type: select
#. Choices
#: ../foomatic-filters.templates:4001
msgid "Custom"
msgstr "Personalitza"

#. Type: select
#. Description
#: ../foomatic-filters.templates:4002
msgid "Command for converting text files to PostScript:"
msgstr "Ordre per convertir fitxers de text a PostScript:"

#. Type: select
#. Description
#: ../foomatic-filters.templates:4002
msgid ""
"If you select 'Automagic', Foomatic will search for one of a2ps, mpage, and "
"enscript (in that order) each time the filter script is executed."
msgstr ""
"Si seleccioneu «Automàgic», Foomatic intentarà trobar a2ps, mpage o enscript "
"(en aquest odre) cada vegada que l'script de filtratge s'executi."

#. Type: select
#. Description
#: ../foomatic-filters.templates:4002
msgid ""
"Please make sure that the selected command is actually available; otherwise "
"print jobs may get lost."
msgstr ""
"Assegureu-vos que l'ordre seleccionada està realment disponible; altrament "
"podeu perdre tasques d'impressió."

#. Type: select
#. Description
#: ../foomatic-filters.templates:4002
msgid ""
"This setting is ignored when foomatic-filters is used with CUPS; instead, "
"the texttops program included in the cups package is always used to convert "
"jobs submitted as plain text to PostScript for printing to raster devices."
msgstr ""
"Aquest paràmetre s'ignora quan foomatic-filters s'utilitza juntament amb "
"CUPS; el programa texttops, inclòs al paquet cups, s'utilitza sempre per "
"convertir a PostScript les tasques d'impressió enviades com text pla."

#. Type: string
#. Description
#: ../foomatic-filters.templates:5001
msgid "Command to convert standard input to PostScript:"
msgstr "Ordre per convertir l'entrada estàndard a PostScript:"

#. Type: string
#. Description
#: ../foomatic-filters.templates:5001
msgid ""
"Please enter the full command line of a command that converts text from "
"standard input to PostScript on standard output."
msgstr ""
"Introduïu l'ordre de línia de comandes que convertirà el text de l'entrada "
"estàndard a PostScript per la sortida estàndard."

#. Type: string
#. Description
#: ../foomatic-filters.templates:5001
msgid ""
"Please note that entering an invalid command line here may result in lost "
"print jobs."
msgstr ""
"Fixeu-vos que introduir una ordre invàlida pot fer-vos perdre tasques "
"d'impressió."

#. Type: boolean
#. Description
#: ../foomatic-filters.templates:6001
msgid "Enable PostScript accounting for CUPS?"
msgstr "Voleu activar el comptatge PostScript per a CUPS?"

#. Type: boolean
#. Description
#: ../foomatic-filters.templates:6001
msgid ""
"You should choose this option if you want to insert PostScript code for "
"accounting into each print job. This is currently only useful with CUPS."
msgstr ""
"Activeu aquesta opció si voleu inserir codi PostScript de comptatge a cada "
"tasca d'impressió. Actualment això només és útil amb CUPS."

#. Type: select
#. Description
#: ../foomatic-filters.templates:7001
msgid "Printer spooler backend for Foomatic:"
msgstr "Cua d'impressió per a Foomatic:"

#. Type: select
#. Description
#: ../foomatic-filters.templates:7001
msgid ""
"Foomatic normally requires a printer spooler (like CUPS or LPRng) to handle "
"communication with the printer and manage print jobs. If no spooler is "
"installed, you can use the 'direct' backend, but this is only recommended "
"for single-user systems."
msgstr ""
"Foomatic normalment requereix una cua d'impressió (com CUPS o LPRng) per "
"gestionar la comunicació amb la impressora i administrar les tasques "
"d'impressió. Si no n'hi ha cap d'instal·lada, podeu utilitzar la interfície "
"«direct», però això només es recomana en sistemes d'un sol usuari."

#. Type: select
#. Description
#: ../foomatic-filters.templates:7001
msgid ""
"The installation process may have already detected the correct spooler; "
"however, if this is the initial installation of this system, or if more than "
"one spooler is installed, the detected spooler may be incorrect."
msgstr ""
"És possible que el procés d'instal·lació hagi detectat la cua d'impressió "
"correctament; tanmateix, si aquesta és la instal·lació inicial del sistema, "
"o si s'ha instal·lat més d'una cua, és possible que la detecció no sigui "
"correcta."

#~ msgid ""
#~ "When used with generic PostScript printers (and under certain conditions "
#~ "with other printers) this causes an extra page to be printed after each "
#~ "job."
#~ msgstr ""
#~ "Quan s'utilitza amb impressores PostScript genèriques (i sota certes "
#~ "condicions amb altres impressores), això provoca la impressió d'una "
#~ "pàgina addicional després de cada tasca."

#~ msgid "Ghostscript interpreter to be used by Foomatic:"
#~ msgstr "Intèrpret Ghostscript que hauria d'utilitzar Foomatic:"

#~ msgid ""
#~ "For non-PostScript printers, print jobs are usually translated from "
#~ "PostScript to the printer's command language using the free Ghostscript "
#~ "interpreter."
#~ msgstr ""
#~ "Per impressores que no siguin PostScript, les tasques d'impressió són "
#~ "normalment traduïdes de PostScript al llenguatge de la impressora "
#~ "utilitzant l'intèrpret lliure Ghostscript."

#~ msgid ""
#~ "There are a number of different versions of the Ghostscript interpreter "
#~ "available. Normally, Foomatic will use the default version (configured by "
#~ "the 'gs' alternative, which can be changed with 'update-alternatives --"
#~ "config gs').  However, you may want to use a different Ghostscript for "
#~ "screen display than for printing; 'gs-esp' is usually a good choice for "
#~ "printing."
#~ msgstr ""
#~ "Hi ha diverses versions de l'intèrpret Ghostscript disponibles. "
#~ "Normalment, Foomatic utilitzarà la versió predeterminada (que es "
#~ "configura mitjançant l'alternativa «gs», i que es pot canviar amb l'ordre "
#~ "«update-alternatives --config gs»). Tanmateix, és possible que vulgueu "
#~ "utilitzar un Ghostscript diferent per visualitzar per la pantalla que per "
#~ "imprimir; «gs-esp» és normalment una bona opció per imprimir."

#~ msgid ""
#~ "You should use the Custom option if you have a locally-installed "
#~ "Ghostscript interpreter."
#~ msgstr ""
#~ "Hauríeu de triar l'opció «Personalitza» si teniu un intèrpret Ghostscript "
#~ "instal·lat localment."

#~ msgid "Custom Ghostscript interpreter path:"
#~ msgstr "Camí a l'intèrpret Ghostscript personalitzat:"

#~ msgid "Please enter the full path to the custom Ghostscript interpreter."
#~ msgstr "Introduïu el camí complet a l'intèrpret Ghostscript personalitzat."

#~ msgid "Example: /opt/artifex.com-ghostscript/bin/gs"
#~ msgstr "Exemple: /opt/artifex.com-ghostscript/bin/gs"
